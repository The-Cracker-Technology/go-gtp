rm -rf /opt/ANDRAX/gw-tester

mkdir /opt/ANDRAX/gw-tester

cp -Rf andraxbin/* /opt/ANDRAX/bin

WORKDIR=$(pwd)

cd examples/gw-tester/enb

go build

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "GO Build enb... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip enb

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip enb... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf enb enb.yml /opt/ANDRAX/gw-tester

cd $WORKDIR

cd examples/gw-tester/mme

go build

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "GO Build mme... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip mme

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip mme... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf mme mme.yml /opt/ANDRAX/gw-tester

cd $WORKDIR

cd examples/gw-tester/pgw

go build

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "GO Build pgw... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip pgw

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip pgw... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf pgw pgw.yml /opt/ANDRAX/gw-tester

cd $WORKDIR

cd examples/gw-tester/sgw

go build

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "GO Build sgw... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip sgw

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip sgw... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf sgw sgw.yml /opt/ANDRAX/gw-tester

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
